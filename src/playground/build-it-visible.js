
class VisibilityToggle extends React.Component{
  constructor(props) {
    super(props);
    this.toggleVisibility = this.toggleVisibility.bind(this);
    this.state={
      visibility: false
    }
  }
  toggleVisibility() {
    this.setState((prevState) => {
      return {
        visibility: !prevState.visibility
      }
    })
  }
  render() {
    return (
      <div>
        <h1>VisibilityToggle</h1>
        <button onClick={this.toggleVisibility}>{this.state.visibility ? 'Hide details' : 'Show Details'}</button>
        {this.state.visibility && <p>These are details visible</p> }
      </div>
    )
  }
}
ReactDOM.render(<VisibilityToggle />, document.getElementById('app'));

// const app = {
//   title: 'Visibility Toggle',
//   details: 'This is thge detalis you can see',
//   isShowed: false
// }
// const onClicked = () => {
//   app.isShowed = !app.isShowed;
//   render();
// }


// const appRoot = document.getElementById('app');


// const render= () =>{
//   const template = (
//     <div>
//       <h1>{app.title}</h1>
//       <button onClick={onClicked}>{app.isShowed ? 'Hide details' : 'Show details'}</button>
//       {app.isShowed && <p>{app.details}</p>}
//     </div>
    
//   );
//   ReactDOM.render(template, appRoot);
// };

// render();